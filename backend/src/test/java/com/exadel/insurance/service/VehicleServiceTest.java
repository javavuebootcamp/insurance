package com.exadel.insurance.service;

import com.exadel.insurance.dto.VehicleDto;
import com.exadel.insurance.entity.Vehicle;
import com.exadel.insurance.repository.VehicleRepository;
import com.exadel.insurance.service.impl.VehicleServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class VehicleServiceTest {
    public static final Long ID = 1L;

    @Mock
    private VehicleRepository vehicleRepository;

    @InjectMocks
    private VehicleServiceImpl vehicleServiceImpl;

    private Vehicle vehicleMock;

    @BeforeEach
    public void setUp() {
        vehicleMock = Vehicle.builder()
                .id(ID)
                .brandName("bmw")
                .build();
    }

    @Test
    public void testFindAll() {
        List<Vehicle> vehiclesMock = new ArrayList<>();
        vehiclesMock.add(Vehicle.builder().id(1L).build());
        vehiclesMock.add(Vehicle.builder().id(2L).build());
        vehiclesMock.add(Vehicle.builder().id(3L).build());

        when(vehicleRepository.findAll()).thenReturn(vehiclesMock);

        List<VehicleDto> vehiclesDto = vehicleServiceImpl.findAll();

        assertNotNull(vehiclesDto);
        assertEquals(3, vehiclesDto.size());
    }

    @Test
    public void testFindById() {
        when(vehicleRepository.findById(anyLong())).thenReturn(Optional.of(vehicleMock));

        VehicleDto vehicleDto = vehicleServiceImpl.findById(ID);

        assertNotNull(vehicleDto);
        assertEquals(ID, vehicleDto.getId());
    }

    @Test
    public void testFindByIdNotFound() {
        when(vehicleRepository.findById(anyLong())).thenReturn(Optional.empty());

        VehicleDto vehicleDto = vehicleServiceImpl.findById(ID);

        assertNull(vehicleDto);
    }


    @Test
    public void testSave() {
        when(vehicleRepository.save(any())).thenReturn(vehicleMock);

        VehicleDto vehicleDto = vehicleServiceImpl.save(VehicleServiceImpl.vehicleToVehicleDto(vehicleMock));

        assertNotNull(vehicleDto);
        verify(vehicleRepository).save(any());
    }

    @Test
    public void testDeleteById() {
        vehicleServiceImpl.deleteById(ID);

        verify(vehicleRepository, times(1)).deleteById(anyLong());
    }
}
