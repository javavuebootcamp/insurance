package com.exadel.insurance.service;

import com.exadel.insurance.dto.ContractDto;
import com.exadel.insurance.entity.Contract;
import com.exadel.insurance.repository.ContractRepository;
import com.exadel.insurance.service.impl.ContractServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ContractServiceTest {
    public static final Long ID = 1L;

    @Mock
    private ContractRepository contractRepository;

    @InjectMocks
    private ContractServiceImpl contractServiceImpl;

    private Contract contractMock;

    @BeforeEach
    public void setUp() {
        contractMock = Contract.builder()
                .id(ID)
                .startDate(LocalDate.of(2022, 5, 15))
                .build();
    }

    @Test
    public void testFindAll() {
        List<Contract> contractsMock = new ArrayList<>();
        contractsMock.add(Contract.builder().id(1L).build());
        contractsMock.add(Contract.builder().id(2L).build());
        contractsMock.add(Contract.builder().id(3L).build());

        when(contractRepository.findAll()).thenReturn(contractsMock);

        List<ContractDto> contractsDto = contractServiceImpl.findAll();

        assertNotNull(contractsDto);
        assertEquals(3, contractsDto.size());
    }

    @Test
    public void testFindById() {
        when(contractRepository.findById(anyLong())).thenReturn(Optional.of(contractMock));

        ContractDto contractDto = contractServiceImpl.findById(ID);

        assertNotNull(contractDto);
        assertEquals(ID, contractDto.getId());
    }

    @Test
    public void testFindByIdNotFound() {
        when(contractRepository.findById(anyLong())).thenReturn(Optional.empty());

        ContractDto contractDto = contractServiceImpl.findById(ID);

        assertNull(contractDto);
    }


    @Test
    public void testSave() {
        when(contractRepository.save(any())).thenReturn(contractMock);

        ContractDto contractDto = contractServiceImpl.save(ContractServiceImpl.contractToContractDto(contractMock));

        assertNotNull(contractDto);
        verify(contractRepository).save(any());
    }

    @Test
    public void testDeleteById() {
        contractServiceImpl.deleteById(ID);

        verify(contractRepository, times(1)).deleteById(anyLong());
    }
}
