package com.exadel.insurance.service;

import com.exadel.insurance.dto.CompanyDto;
import com.exadel.insurance.entity.Company;
import com.exadel.insurance.repository.CompanyRepository;
import com.exadel.insurance.service.impl.CompanyServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CompanyServiceTest {
    public static final Long ID = 1L;

    @Mock
    private CompanyRepository companyRepository;

    @InjectMocks
    private CompanyServiceImpl companyServiceImpl;

    private Company companyMock;

    @BeforeEach
    public void setUp() {
        companyMock = Company.builder()
                .id(ID)
                .name("Apple")
                .build();
    }

    @Test
    public void testFindAll() {
        List<Company> companysMock = new ArrayList<>();
        companysMock.add(Company.builder().id(1L).build());
        companysMock.add(Company.builder().id(2L).build());
        companysMock.add(Company.builder().id(3L).build());

        when(companyRepository.findAll()).thenReturn(companysMock);

        List<CompanyDto> companysDto = companyServiceImpl.findAll();

        assertNotNull(companysDto);
        assertEquals(3, companysDto.size());
    }

    @Test
    public void testFindById() {
        when(companyRepository.findById(anyLong())).thenReturn(Optional.of(companyMock));

        CompanyDto companyDto = companyServiceImpl.findById(ID);

        assertNotNull(companyDto);
        assertEquals(ID, companyDto.getId());
    }

    @Test
    public void testFindByIdNotFound() {
        when(companyRepository.findById(anyLong())).thenReturn(Optional.empty());

        CompanyDto companyDto = companyServiceImpl.findById(ID);

        assertNull(companyDto);
    }


    @Test
    public void testSave() {
        when(companyRepository.save(any())).thenReturn(companyMock);

        CompanyDto companyDto = companyServiceImpl.save(CompanyServiceImpl.companyToCompanyDto(companyMock));

        assertNotNull(companyDto);
        verify(companyRepository).save(any());
    }

    @Test
    public void testDeleteById() {
        companyServiceImpl.deleteById(ID);

        verify(companyRepository, times(1)).deleteById(anyLong());
    }
}
