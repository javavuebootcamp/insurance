package com.exadel.insurance.service;

import com.exadel.insurance.dto.OwnerDto;
import com.exadel.insurance.entity.Owner;
import com.exadel.insurance.repository.OwnerRepository;
import com.exadel.insurance.service.impl.OwnerServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class OwnerServiceTest {
    public static final Long ID = 1L;

    @Mock
    private OwnerRepository ownerRepository;

    @InjectMocks
    private OwnerServiceImpl ownerServiceImpl;

    private Owner ownerMock;

    @BeforeEach
    public void setUp() {
        ownerMock = Owner.builder()
                .id(ID)
                .firstName("Jan")
                .build();
    }

    @Test
    public void testFindAll() {
        List<Owner> ownersMock = new ArrayList<>();
        ownersMock.add(Owner.builder().id(1L).build());
        ownersMock.add(Owner.builder().id(2L).build());
        ownersMock.add(Owner.builder().id(3L).build());

        when(ownerRepository.findAll()).thenReturn(ownersMock);

        List<OwnerDto> ownersDto = ownerServiceImpl.findAll();

        assertNotNull(ownersDto);
        assertEquals(3, ownersDto.size());
    }

    @Test
    public void testFindById() {
        when(ownerRepository.findById(anyLong())).thenReturn(Optional.of(ownerMock));

        OwnerDto ownerDto = ownerServiceImpl.findById(ID);

        assertNotNull(ownerDto);
        assertEquals(ID, ownerDto.getId());
    }

    @Test
    public void testFindByIdNotFound() {
        when(ownerRepository.findById(anyLong())).thenReturn(Optional.empty());

        OwnerDto ownerDto = ownerServiceImpl.findById(ID);

        assertNull(ownerDto);
    }


    @Test
    public void testSave() {
        when(ownerRepository.save(any())).thenReturn(ownerMock);

        OwnerDto ownerDto = ownerServiceImpl.save(OwnerServiceImpl.ownerToOwnerDto(ownerMock));

        assertNotNull(ownerDto);
        verify(ownerRepository).save(any());
    }

    @Test
    public void testDeleteById() {
        ownerServiceImpl.deleteById(ID);

        verify(ownerRepository, times(1)).deleteById(anyLong());
    }
}
