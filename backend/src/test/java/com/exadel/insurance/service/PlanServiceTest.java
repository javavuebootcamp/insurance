package com.exadel.insurance.service;

import com.exadel.insurance.dto.PlanDto;
import com.exadel.insurance.entity.Plan;
import com.exadel.insurance.repository.PlanRepository;
import com.exadel.insurance.service.impl.PlanServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class PlanServiceTest {
    public static final Long ID = 1L;

    @Mock
    private PlanRepository planRepository;

    @InjectMocks
    private PlanServiceImpl planServiceImpl;

    private Plan planMock;

    @BeforeEach
    public void setUp() {
        planMock = Plan.builder()
                .id(ID)
                .condition("good")
                .build();
    }

    @Test
    public void testFindAll() {
        List<Plan> plansMock = new ArrayList<>();
        plansMock.add(Plan.builder().id(1L).build());
        plansMock.add(Plan.builder().id(2L).build());
        plansMock.add(Plan.builder().id(3L).build());

        when(planRepository.findAll()).thenReturn(plansMock);

        List<PlanDto> plansDto = planServiceImpl.findAll();

        assertNotNull(plansDto);
        assertEquals(3, plansDto.size());
    }

    @Test
    public void testFindById() {
        when(planRepository.findById(anyLong())).thenReturn(Optional.of(planMock));

        PlanDto planDto = planServiceImpl.findById(ID);

        assertNotNull(planDto);
        assertEquals(ID, planDto.getId());
    }

    @Test
    public void testFindByIdNotFound() {
        when(planRepository.findById(anyLong())).thenReturn(Optional.empty());

        PlanDto planDto = planServiceImpl.findById(ID);

        assertNull(planDto);
    }


    @Test
    public void testSave() {
        when(planRepository.save(any())).thenReturn(planMock);

        PlanDto planDto = planServiceImpl.save(PlanServiceImpl.planToPlanDto(planMock));

        assertNotNull(planDto);
        verify(planRepository).save(any());
    }

    @Test
    public void testDeleteById() {
        planServiceImpl.deleteById(ID);

        verify(planRepository, times(1)).deleteById(anyLong());
    }
}
