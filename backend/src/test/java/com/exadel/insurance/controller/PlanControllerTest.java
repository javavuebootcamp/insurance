package com.exadel.insurance.controller;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import com.exadel.insurance.entity.Plan;
import com.exadel.insurance.repository.PlanRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
@SpringBootTest
@AutoConfigureMockMvc
public class PlanControllerTest {
    private static final Plan PLAN = Plan.builder().build();
    private static final String URL = "/plans";
    private Long id = null;

    @Autowired
    private PlanRepository planRepository;
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;


    @BeforeEach
    public void createPlanForTest() {
        id = planRepository.save(PLAN).getId();
    }

    @AfterEach
    public void deletePlanForTest() {
        planRepository.deleteById(id);
    }

    @Test
    public void testCreatePlan() throws Exception {
        mockMvc.perform(
                        post(URL)
                                .content(objectMapper.writeValueAsString(PLAN))
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").isNumber());
    }

    @Test
    public void testUpdatePlan() throws Exception {
        PLAN.setId(id);
        System.out.println(id);
        mockMvc.perform(
                        put(URL+"/{id}", id)
                                .content(objectMapper.writeValueAsString(PLAN))
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(id));
    }

    @Test
    public void testGetPlanById() throws Exception {

        mockMvc.perform(
                        get(URL + "/{id}", id))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(id));
    }

    @Test
    public void testGetPlans() throws Exception {
        mockMvc.perform(
                        get(URL))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray());
    }

    @Test
    public void testGetPlansByObjectId() throws Exception {
        mockMvc.perform(
                        get(URL + "/{objectId}", 1L))
                .andExpect(status().isOk())
                .andExpect(jsonPath("*").isArray());
    }

    @Test
    public void testGetPlanNotFound() throws Exception {
        mockMvc.perform(
                        get(URL + "/0"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDeletePlanById() throws Exception {
        mockMvc.perform(
                        delete(URL + "/{id}", id))
                .andExpect(status().isOk());
    }

    @Test
    public void testDeletePlanNotFound() throws Exception {
        mockMvc.perform(
                        delete(URL + "/0"))
                .andExpect(status().isNotFound());
    }
}
