package com.exadel.insurance.controller;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import com.exadel.insurance.entity.Company;
import com.exadel.insurance.repository.CompanyRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
@SpringBootTest
@AutoConfigureMockMvc
public class CompanyControllerTest {
    private static final Company COMPANY = Company.builder().build();
    private static final String URL = "/companys";
    private Long id = null;

    @Autowired
    private CompanyRepository companyRepository;
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;


    @BeforeEach
    public void createCompanyForTest() {
        id = companyRepository.save(COMPANY).getId();
    }

    @AfterEach
    public void deleteCompanyForTest() {
        companyRepository.deleteById(id);
    }

    @Test
    public void testCreateCompany() throws Exception {
        mockMvc.perform(
                        post(URL)
                                .content(objectMapper.writeValueAsString(COMPANY))
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").isNumber());
    }

    @Test
    public void testUpdateCompany() throws Exception {
        COMPANY.setId(id);
        System.out.println(id);
        mockMvc.perform(
                        put(URL+"/{id}", id)
                                .content(objectMapper.writeValueAsString(COMPANY))
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(id));
    }

    @Test
    public void testGetCompanyById() throws Exception {

        mockMvc.perform(
                        get(URL + "/{id}", id))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(id));
    }

    @Test
    public void testGetCompanys() throws Exception {
        mockMvc.perform(
                        get(URL))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray());
    }

    @Test
    public void testGetCompanysByObjectId() throws Exception {
        mockMvc.perform(
                        get(URL + "/{objectId}", 1L))
                .andExpect(status().isOk())
                .andExpect(jsonPath("*").isArray());
    }

    @Test
    public void testGetCompanyNotFound() throws Exception {
        mockMvc.perform(
                        get(URL + "/0"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDeleteCompanyById() throws Exception {
        mockMvc.perform(
                        delete(URL + "/{id}", id))
                .andExpect(status().isOk());
    }

    @Test
    public void testDeleteCompanyNotFound() throws Exception {
        mockMvc.perform(
                        delete(URL + "/0"))
                .andExpect(status().isNotFound());
    }

}
