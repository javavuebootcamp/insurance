package com.exadel.insurance.controller;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import com.exadel.insurance.entity.Contract;
import com.exadel.insurance.repository.ContractRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
@SpringBootTest
@AutoConfigureMockMvc
public class ContractControllerTest {
    private static final Contract CONTRACT = Contract.builder().build();
    private static final String URL = "/contracts";
    private Long id = null;

    @Autowired
    private ContractRepository contractRepository;
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;


    @BeforeEach
    public void createContractForTest() {
        id = contractRepository.save(CONTRACT).getId();
    }

    @AfterEach
    public void deleteContractForTest() {
        contractRepository.deleteById(id);
    }

    @Test
    public void testCreateContract() throws Exception {
        mockMvc.perform(
                        post(URL)
                                .content(objectMapper.writeValueAsString(CONTRACT))
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").isNumber());
    }

    @Test
    public void testUpdateContract() throws Exception {
        CONTRACT.setId(id);
        mockMvc.perform(
                        put(URL+"/{id}", id)
                                .content(objectMapper.writeValueAsString(CONTRACT))
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(id));
    }

    @Test
    public void testGetContractById() throws Exception {

        mockMvc.perform(
                        get(URL + "/{id}", id))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(id));
    }

    @Test
    public void testGetContracts() throws Exception {
        mockMvc.perform(
                        get(URL))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray());
    }

    @Test
    public void testGetContractsByObjectId() throws Exception {
        mockMvc.perform(
                        get(URL + "/{objectId}", 1L))
                .andExpect(status().isOk())
                .andExpect(jsonPath("*").isArray());
    }

    @Test
    public void testGetContractNotFound() throws Exception {
        mockMvc.perform(
                        get(URL + "/0"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDeleteContractById() throws Exception {
        mockMvc.perform(
                        delete(URL + "/{id}", id))
                .andExpect(status().isOk());
    }

    @Test
    public void testDeleteContractNotFound() throws Exception {
        mockMvc.perform(
                        delete(URL + "/0"))
                .andExpect(status().isNotFound());
    }
}
