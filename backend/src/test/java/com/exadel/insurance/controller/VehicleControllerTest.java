package com.exadel.insurance.controller;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import com.exadel.insurance.entity.Vehicle;
import com.exadel.insurance.repository.VehicleRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
@SpringBootTest
@AutoConfigureMockMvc
public class VehicleControllerTest {
    private static final Vehicle VEHICLE = Vehicle.builder().build();
    private static final String URL = "/vehicles";
    private Long id = null;

    @Autowired
    private VehicleRepository vehicleRepository;
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;


    @BeforeEach
    public void createVehicleForTest() {
        id = vehicleRepository.save(VEHICLE).getId();
    }

    @AfterEach
    public void deleteVehicleForTest() {
        vehicleRepository.deleteById(id);
    }

    @Test
    public void testCreateVehicle() throws Exception {
        mockMvc.perform(
                        post(URL)
                                .content(objectMapper.writeValueAsString(VEHICLE))
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").isNumber());
    }

    @Test
    public void testUpdateVehicle() throws Exception {
        VEHICLE.setId(id);
        mockMvc.perform(
                        put(URL+"/{id}", id)
                                .content(objectMapper.writeValueAsString(VEHICLE))
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(id));
    }

    @Test
    public void testGetVehicleById() throws Exception {

        mockMvc.perform(
                        get(URL + "/{id}", id))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(id));
    }

    @Test
    public void testGetVehicles() throws Exception {
        mockMvc.perform(
                        get(URL))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray());
    }

    @Test
    public void testGetVehiclesByObjectId() throws Exception {
        mockMvc.perform(
                        get(URL + "/{objectId}", 1L))
                .andExpect(status().isOk())
                .andExpect(jsonPath("*").isArray());
    }

    @Test
    public void testGetVehicleNotFound() throws Exception {
        mockMvc.perform(
                        get(URL + "/0"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDeleteVehicleById() throws Exception {
        mockMvc.perform(
                        delete(URL + "/{id}", id))
                .andExpect(status().isOk());
    }

    @Test
    public void testDeleteVehicleNotFound() throws Exception {
        mockMvc.perform(
                        delete(URL + "/0"))
                .andExpect(status().isNotFound());
    }
}
