package com.exadel.insurance.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.time.LocalDate;

@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "contracts")
public class Contract extends BaseEntity {
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "plan_id")
    @JsonBackReference(value = "plan-contract")
    private Plan plan;
  //  @ManyToOne(fetch = FetchType.LAZY)
    @OneToOne(mappedBy = "contract", cascade = CascadeType.ALL)
   // @JoinColumn(name = "vehicle_id")
    //@JsonBackReference(value = "vehicle-contract")
    private Vehicle vehicle;
    @Column(name = "start_date")
    private LocalDate startDate;
    @Column(name = "end_date")
    private LocalDate endDate;
}
