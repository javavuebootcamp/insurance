package com.exadel.insurance.dto;

import com.exadel.insurance.entity.Address;
import com.exadel.insurance.entity.Vehicle;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class OwnerDto {
    private Long id;
    private String firstName;
    private String lastname;
    private int age;
    private String phoneNumber;
    private String email;
    private String password;
    private Address address;
    private List<Vehicle> vehicles;
}
