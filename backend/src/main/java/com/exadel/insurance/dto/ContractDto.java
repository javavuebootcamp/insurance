package com.exadel.insurance.dto;

import com.exadel.insurance.entity.Plan;
import com.exadel.insurance.entity.Vehicle;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.time.LocalDate;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class ContractDto {
    private Long id;
    private Plan plan;
    private Vehicle vehicle;
    private LocalDate startDate;
    private LocalDate endDate;
}
