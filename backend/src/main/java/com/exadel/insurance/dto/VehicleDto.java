package com.exadel.insurance.dto;

import com.exadel.insurance.entity.Contract;
import com.exadel.insurance.entity.Owner;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class VehicleDto {
    private Long id;
    private String brandName;
    private String model;
    private Owner owner;
    private Contract contract;
}
