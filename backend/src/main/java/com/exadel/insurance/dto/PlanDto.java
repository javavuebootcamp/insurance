package com.exadel.insurance.dto;

import com.exadel.insurance.entity.Company;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.math.BigDecimal;
import java.time.Period;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class PlanDto {
    private Long id;
    private BigDecimal price;
    private Period duration;
    private String condition;
    private Company company;
}
