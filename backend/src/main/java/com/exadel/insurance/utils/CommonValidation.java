package com.exadel.insurance.utils;

import lombok.extern.slf4j.Slf4j;

import java.util.Objects;
@Slf4j
public class CommonValidation {
    public static void isNullCheck(Object checkingObj, String objectMethodName){
        if (Objects.isNull(checkingObj)) {
            log.error("empty" + objectMethodName + "object");
            throw new RuntimeException("empty" + objectMethodName + "object");
        }
    }
}
