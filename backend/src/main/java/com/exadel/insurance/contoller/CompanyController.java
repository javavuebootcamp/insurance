package com.exadel.insurance.contoller;

import com.exadel.insurance.dto.CompanyDto;
import com.exadel.insurance.exception.NotFoundException;
import com.exadel.insurance.service.CompanyService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/companys")
public class CompanyController {
    private final CompanyService companyService;

    @GetMapping("/count")
    public Long getCompanysCount() {
        log.info("get companys count");
        return companyService.count();
    }

    @GetMapping
    public List<CompanyDto> getAllCompany() {
        log.info("get all companys");
        return companyService.findAll();
    }

    @GetMapping("/{companyId}")
    public CompanyDto getCompanyById(@PathVariable Long companyId) {
        log.info("get Company by companyId companyId={}", companyId);
        existCheck(companyId);
        return companyService.findById(companyId);
    }

    @PostMapping
    public CompanyDto saveCompany(@RequestBody CompanyDto companyDto) {
        log.info("save new company company={}", companyDto);
        return companyService.save(companyDto);
    }

    @PutMapping("/{companyId}")
    public CompanyDto updateCompany(@RequestBody CompanyDto companyDto, @PathVariable Long companyId) {
        log.info("update company Company={}", companyDto);
        existCheck(companyId);
        companyDto.setId(companyId);
        return companyService.save(companyDto);
    }

    @DeleteMapping("/{companyId}")
    public void deleteCompanyById(@PathVariable Long companyId) {
        log.info("delete company by companyId={}", companyId);
        existCheck(companyId);
        companyService.deleteById(companyId);
    }

    private void existCheck(Long companyId) {
        CompanyDto companyDto = companyService.findById(companyId);

        if (Objects.isNull(companyDto)) {
            throw new NotFoundException("Company not found");
        }
    }
}
