package com.exadel.insurance.contoller;

import com.exadel.insurance.dto.ContractDto;
import com.exadel.insurance.exception.NotFoundException;
import com.exadel.insurance.service.ContractService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/contracts")
public class ContractController {
    private final ContractService contractService;

    @GetMapping("/count")
    public Long getContractsCount() {
        log.info("get Contracts count");
        return contractService.count();
    }

    @GetMapping
    public List<ContractDto> getAllContract() {
        log.info("get all Contracts");
        return contractService.findAll();
    }

    @GetMapping("/{contractId}")
    public ContractDto getContractById(@PathVariable Long contractId) {
        log.info("get Contract by contractId contractId={}", contractId);
        existCheck(contractId);
        return contractService.findById(contractId);
    }

    @PostMapping
    public ContractDto saveContract(@RequestBody ContractDto contractDto) {
        log.info("save new contract contract={}", contractDto);
        return contractService.save(contractDto);
    }

    @PutMapping("/{contractId}")
    public ContractDto updateContract(@RequestBody ContractDto contractDto, @PathVariable Long contractId) {
        log.info("update contract contract={}", contractDto);
        existCheck(contractId);
        contractDto.setId(contractId);
        return contractService.save(contractDto);
    }

    @DeleteMapping("/{contractId}")
    public void deleteContractById(@PathVariable Long contractId) {
        log.info("delete Contract by contractId={}", contractId);
        existCheck(contractId);
        contractService.deleteById(contractId);
    }

    private void existCheck(Long contractId) {
        ContractDto contractDto = contractService.findById(contractId);

        if (Objects.isNull(contractDto)) {
            throw new NotFoundException("Contract not found");
        }
    }
}
