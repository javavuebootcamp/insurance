package com.exadel.insurance.contoller;

import com.exadel.insurance.dto.PlanDto;
import com.exadel.insurance.exception.NotFoundException;
import com.exadel.insurance.service.PlanService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/plans")
public class PlanController {
    private final PlanService planService;

    @GetMapping("/count")
    public Long getPlansCount() {
        log.info("get Plans count");
        return planService.count();
    }

    @GetMapping
    public List<PlanDto> getAllPlan() {
        log.info("get all Plans");
        return planService.findAll();
    }

    @GetMapping("/{planId}")
    public PlanDto getPlanById(@PathVariable Long planId) {
        log.info("get Plan by planId planId={}", planId);
        existCheck(planId);
        return planService.findById(planId);
    }

    @PostMapping
    public PlanDto savePlan(@RequestBody PlanDto planDto) {
        log.info("save new plan plan={}", planDto);
        return planService.save(planDto);
    }

    @PutMapping("/{planId}")
    public PlanDto updatePlan(@RequestBody PlanDto planDto, @PathVariable Long planId) {
        log.info("update plan plan={}", planDto);
        existCheck(planId);
        planDto.setId(planId);
        return planService.save(planDto);
    }

    @DeleteMapping("/{planId}")
    public void deletePlanById(@PathVariable Long planId) {
        log.info("delete Plan by planId={}", planId);
        existCheck(planId);
        planService.deleteById(planId);
    }

    private void existCheck(Long planId) {
        PlanDto planDto = planService.findById(planId);

        if (Objects.isNull(planDto)) {
            throw new NotFoundException("Plan not found");
        }
    }
}
