package com.exadel.insurance.contoller;

import com.exadel.insurance.dto.VehicleDto;
import com.exadel.insurance.exception.NotFoundException;
import com.exadel.insurance.service.VehicleService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/vehicles")
public class VehicleController {
    private final VehicleService vehicleService;

    @GetMapping("/count")
    public Long getVehiclesCount() {
        log.info("get Vehicles count");
        return vehicleService.count();
    }

    @GetMapping
    public List<VehicleDto> getAllVehicle() {
        log.info("get all Vehicles");
        return vehicleService.findAll();
    }

    @GetMapping("/{vehicleId}")
    public VehicleDto getVehicleById(@PathVariable Long vehicleId) {
        log.info("get Vehicle by vehicleId vehicleId={}", vehicleId);
        existCheck(vehicleId);
        return vehicleService.findById(vehicleId);
    }

    @PostMapping
    public VehicleDto saveVehicle(@RequestBody VehicleDto vehicleDto) {
        log.info("save new vehicle vehicle={}", vehicleDto);
        return vehicleService.save(vehicleDto);
    }

    @PutMapping("/{vehicleId}")
    public VehicleDto updateVehicle(@RequestBody VehicleDto vehicleDto, @PathVariable Long vehicleId) {
        log.info("update vehicle vehicle={}", vehicleDto);
        existCheck(vehicleId);
        vehicleDto.setId(vehicleId);
        return vehicleService.save(vehicleDto);
    }

    @DeleteMapping("/{vehicleId}")
    public void deleteVehicleById(@PathVariable Long vehicleId) {
        log.info("delete Vehicle by vehicleId={}", vehicleId);
        existCheck(vehicleId);
        vehicleService.deleteById(vehicleId);
    }

    private void existCheck(Long vehicleId) {
        VehicleDto vehicleDto = vehicleService.findById(vehicleId);

        if (Objects.isNull(vehicleDto)) {
            throw new NotFoundException("Vehicle not found");
        }
    }
}
