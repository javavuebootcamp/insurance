package com.exadel.insurance.contoller;

import com.exadel.insurance.dto.OwnerDto;
import com.exadel.insurance.exception.NotFoundException;
import com.exadel.insurance.service.OwnerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/owners")
public class OwnerController {
    private final OwnerService ownerService;

    @GetMapping("/count")
    public Long getOwnersCount() {
        log.info("get Owners count");
        return ownerService.count();
    }

    @GetMapping
    public List<OwnerDto> getAllOwner() {
        log.info("get all Owners");
        return ownerService.findAll();
    }

    @GetMapping("/{ownerId}")
    public OwnerDto getOwnerById(@PathVariable Long ownerId) {
        log.info("get Owner by ownerId ownerId={}", ownerId);
        existCheck(ownerId);
        return ownerService.findById(ownerId);
    }

    @PostMapping
    public OwnerDto saveOwner(@RequestBody OwnerDto ownerDto) {
        log.info("save new owner owner={}", ownerDto);
        return ownerService.save(ownerDto);
    }

    @PutMapping("/{ownerId}")
    public OwnerDto updateOwner(@RequestBody OwnerDto ownerDto, @PathVariable Long ownerId) {
        log.info("update owner owner={}", ownerDto);
        existCheck(ownerId);
        ownerDto.setId(ownerId);
        return ownerService.save(ownerDto);
    }

    @DeleteMapping("/{ownerId}")
    public void deleteOwnerById(@PathVariable Long ownerId) {
        log.info("delete Owner by ownerId={}", ownerId);
        existCheck(ownerId);
        ownerService.deleteById(ownerId);
    }

    private void existCheck(Long ownerId) {
        OwnerDto ownerDto = ownerService.findById(ownerId);

        if (Objects.isNull(ownerDto)) {
            throw new NotFoundException("Owner not found");
        }
    }
}
