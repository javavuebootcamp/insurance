package com.exadel.insurance.service.impl;

import com.exadel.insurance.dto.ContractDto;
import com.exadel.insurance.dto.ContractDto;
import com.exadel.insurance.entity.Contract;
import com.exadel.insurance.entity.Contract;
import com.exadel.insurance.entity.Contract;
import com.exadel.insurance.entity.Contract;
import com.exadel.insurance.repository.ContractRepository;
import com.exadel.insurance.service.ContractService;
import lombok.RequiredArgsConstructor;
import lombok.Synchronized;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.exadel.insurance.utils.CommonValidation.isNullCheck;

@Slf4j
@Service
@RequiredArgsConstructor
public class ContractServiceImpl implements ContractService {
    private final ContractRepository contractRepository;

    @Override
    @Transactional(readOnly = true)
    public Long count() {
        log.info("get claim contract");
        return contractRepository.count();
    }

    @Override
    @Transactional(readOnly = true)
    public List<ContractDto> findAll() {
        log.info("find all contract");
        return contractRepository.findAll().stream()
                .map(ContractServiceImpl::contractToContractDto)
                .collect(Collectors.toList());

    }

    @Override
    @Transactional(readOnly = true)
    public ContractDto findById(Long id) {
        isNullCheck(id, "contractFind");
        log.info("find Contract by id");
        Contract contract = contractRepository.findById(id).orElse(null);
        return contractToContractDto(contract);
    }

    @Override
    @Transactional
    public ContractDto save(ContractDto contractDto) {
        isNullCheck(contractDto, "contractSave");
        log.info("save contract");
        Contract contract = contractDtoToContract(contractDto);
        Contract updatedContract = contractRepository.save(contract);
        return contractToContractDto(updatedContract);
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        isNullCheck(id, "contractDeleteById");
        log.info("delete contract by id");
        contractRepository.deleteById(id);
    }

    @Synchronized
    public static Contract contractDtoToContract(ContractDto contractDto) {
        if (Objects.isNull(contractDto)) {
            return null;
        }
        return Contract.builder()
                .id(contractDto.getId())
                .plan(contractDto.getPlan())
                .vehicle(contractDto.getVehicle())
                .startDate(contractDto.getStartDate())
                .endDate(contractDto.getEndDate())
                .build();
    }

    @Synchronized
    public static ContractDto contractToContractDto(Contract contract) {
        if (Objects.isNull(contract)) {
            return null;
        }
        return ContractDto.builder()
                .id(contract.getId())
                .plan(contract.getPlan())
                .vehicle(contract.getVehicle())
                .startDate(contract.getStartDate())
                .endDate(contract.getEndDate())
                .build();
    }
}
