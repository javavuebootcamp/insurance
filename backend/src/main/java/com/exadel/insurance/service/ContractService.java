package com.exadel.insurance.service;

import com.exadel.insurance.dto.ContractDto;

public interface ContractService extends CrudService<ContractDto, Long>{
}
