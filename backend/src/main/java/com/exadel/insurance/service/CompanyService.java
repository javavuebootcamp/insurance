package com.exadel.insurance.service;

import com.exadel.insurance.dto.CompanyDto;

public interface CompanyService extends CrudService<CompanyDto, Long> {
}
