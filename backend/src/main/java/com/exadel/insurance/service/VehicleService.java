package com.exadel.insurance.service;

import com.exadel.insurance.dto.VehicleDto;

public interface VehicleService extends CrudService<VehicleDto, Long> {

}
