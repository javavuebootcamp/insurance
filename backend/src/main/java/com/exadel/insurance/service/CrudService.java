package com.exadel.insurance.service;

import java.util.List;

public interface CrudService<T, ID> {
    Long count();

    List<T> findAll();

    T findById(ID id);

    T save(T t);

    void deleteById(ID id);
}
