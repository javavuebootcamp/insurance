package com.exadel.insurance.service.impl;

import com.exadel.insurance.dto.PlanDto;
import com.exadel.insurance.entity.Plan;
import com.exadel.insurance.entity.Plan;
import com.exadel.insurance.repository.PlanRepository;
import com.exadel.insurance.service.PlanService;
import lombok.RequiredArgsConstructor;
import lombok.Synchronized;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.exadel.insurance.utils.CommonValidation.isNullCheck;

@Slf4j
@Service
@RequiredArgsConstructor
public class PlanServiceImpl implements PlanService {
    private final PlanRepository planRepository;

    @Override
    @Transactional(readOnly = true)
    public Long count() {
        log.info("get claim plan");
        return planRepository.count();
    }

    @Override
    @Transactional(readOnly = true)
    public List<PlanDto> findAll() {
        log.info("find all Plan");
        return planRepository.findAll().stream()
                .map(PlanServiceImpl::planToPlanDto)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public PlanDto findById(Long id) {
        isNullCheck(id, "PlanFind");
        log.info("find Plan by id");
        Plan plan = planRepository.findById(id).orElse(null);
        return planToPlanDto(plan);
    }

    @Override
    @Transactional
    public PlanDto save(PlanDto planDto) {
        isNullCheck(planDto, "PlanSave");
        log.info("save Plan");
        Plan plan = planDtoToPlan(planDto);
        Plan updatedPlan = planRepository.save(plan);
        return planToPlanDto(updatedPlan);
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        isNullCheck(id, "PlanDeleteById");
        log.info("delete Plan by id");
        planRepository.deleteById(id);
    }

    @Synchronized
    public static Plan planDtoToPlan(PlanDto planDto) {
        if (Objects.isNull(planDto)) {
            return null;
        }
        return Plan.builder()
                .id(planDto.getId())
                .price(planDto.getPrice())
                .duration(planDto.getDuration())
                .condition(planDto.getCondition())
                .company(planDto.getCompany())
                .build();
    }

    @Synchronized
    public static PlanDto planToPlanDto(Plan plan) {
        if (Objects.isNull(plan)) {
            return null;
        }
        return PlanDto.builder()
                .id(plan.getId())
                .price(plan.getPrice())
                .duration(plan.getDuration())
                .condition(plan.getCondition())
                .company(plan.getCompany())
                .build();
    }
}
