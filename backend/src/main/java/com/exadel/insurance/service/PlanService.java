package com.exadel.insurance.service;

import com.exadel.insurance.dto.PlanDto;

public interface PlanService extends CrudService<PlanDto, Long> {
}
