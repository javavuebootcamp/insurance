package com.exadel.insurance.service.impl;

import com.exadel.insurance.dto.VehicleDto;
import com.exadel.insurance.entity.Vehicle;
import com.exadel.insurance.repository.VehicleRepository;
import com.exadel.insurance.service.VehicleService;
import lombok.RequiredArgsConstructor;
import lombok.Synchronized;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.exadel.insurance.utils.CommonValidation.isNullCheck;

@Slf4j
@Service
@RequiredArgsConstructor
public class VehicleServiceImpl implements VehicleService {
    private final VehicleRepository vehicleRepository;

    @Override
    @Transactional(readOnly = true)
    public Long count() {
        log.info("get claim vehicle");
        return vehicleRepository.count();
    }

    @Override
    @Transactional(readOnly = true)
    public List<VehicleDto> findAll() {
        log.info("find all vehicle");
        return vehicleRepository.findAll().stream()
                .map(VehicleServiceImpl::vehicleToVehicleDto)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public VehicleDto findById(Long id) {
        isNullCheck(id, "vehicleFind");
        log.info("find vehicle by id");
        Vehicle vehicle = vehicleRepository.findById(id).orElse(null);
        return vehicleToVehicleDto(vehicle);
    }

    @Override
    @Transactional
    public VehicleDto save(VehicleDto vehicleDto) {
        isNullCheck(vehicleDto, "vehicleSave");
        log.info("save vehicle");
        Vehicle vehicle = vehicleDtoToVehicle(vehicleDto);
        Vehicle updatedVehicle = vehicleRepository.save(vehicle);
        return vehicleToVehicleDto(updatedVehicle);
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        isNullCheck(id, "vehicleDeleteById");
        log.info("delete vehicle by id");
        vehicleRepository.deleteById(id);
    }

    @Synchronized
    public static Vehicle vehicleDtoToVehicle(VehicleDto vehicleDto) {
        if (Objects.isNull(vehicleDto)) {
            return null;
        }
        return Vehicle.builder()
                .id(vehicleDto.getId())
                .brandName(vehicleDto.getBrandName())
                .model(vehicleDto.getModel())
                .owner(vehicleDto.getOwner())
                .contract(vehicleDto.getContract())
                .build();
    }

    @Synchronized
    public static VehicleDto vehicleToVehicleDto(Vehicle vehicle) {
        if (Objects.isNull(vehicle)) {
            return null;
        }
        return VehicleDto.builder()
                .id(vehicle.getId())
                .brandName(vehicle.getBrandName())
                .model(vehicle.getModel())
                .owner(vehicle.getOwner())
                .contract(vehicle.getContract())
                .build();
    }
}
