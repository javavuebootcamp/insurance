package com.exadel.insurance.service.impl;

import com.exadel.insurance.dto.CompanyDto;
import com.exadel.insurance.entity.Company;
import com.exadel.insurance.repository.CompanyRepository;
import com.exadel.insurance.service.CompanyService;
import jakarta.annotation.Nullable;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Synchronized;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.exadel.insurance.utils.CommonValidation.isNullCheck;

@Slf4j
@Service
@RequiredArgsConstructor
public class CompanyServiceImpl implements CompanyService {

    private final CompanyRepository companyRepository;


    @Override
    @Transactional(readOnly = true)
    public Long count() {
        log.info("get claim company");
        return companyRepository.count();
    }

    @Override
    @Transactional(readOnly = true)
    public List<CompanyDto> findAll() {
        log.info("find all company");
        return companyRepository.findAll().stream()
                .map(CompanyServiceImpl::companyToCompanyDto)
                .collect(Collectors.toList());

    }

    @Override
    @Transactional(readOnly = true)
    public CompanyDto findById(Long id) {
        isNullCheck(id, "companyFind");
        log.info("find company by id");
        Company company = companyRepository.findById(id).orElse(null);
        return companyToCompanyDto(company);
    }

    @Override
    @Transactional
    public CompanyDto save(CompanyDto companyDto) {
        isNullCheck(companyDto, "companySave");
        log.info("save company");
        Company company = companyDtoToCompany(companyDto);
        Company updatedCompany = companyRepository.save(company);
        return companyToCompanyDto(updatedCompany);
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        isNullCheck(id, "companyDeleteById");
        log.info("delete company by id");
        companyRepository.deleteById(id);
    }


    @Synchronized
    public static Company companyDtoToCompany(CompanyDto companyDto) {
        if(Objects.isNull(companyDto)){
            return null;
        }

        return Company.builder()
                .id(companyDto.getId())
                .name(companyDto.getName())
                .description(companyDto.getDescription())
                .plans(companyDto.getPlans())
                .build();
    }

    @Synchronized
    public static CompanyDto companyToCompanyDto(Company company) {
        if(Objects.isNull(company)){
            return null;
        }
        return CompanyDto.builder()
                .id(company.getId())
                .name(company.getName())
                .description(company.getDescription())
                .plans(company.getPlans())
                .build();
    }
}
