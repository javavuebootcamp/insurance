package com.exadel.insurance.service.impl;

import com.exadel.insurance.dto.OwnerDto;
import com.exadel.insurance.entity.Owner;
import com.exadel.insurance.entity.Owner;

import com.exadel.insurance.repository.OwnerRepository;
import com.exadel.insurance.service.OwnerService;
import lombok.RequiredArgsConstructor;
import lombok.Synchronized;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.exadel.insurance.utils.CommonValidation.isNullCheck;

@Slf4j
@Service
@RequiredArgsConstructor
public class OwnerServiceImpl implements OwnerService {
    private final OwnerRepository ownerRepository;

    @Override
    @Transactional(readOnly = true)
    public Long count() {
        log.info("get claim owner");
        return ownerRepository.count();
    }

    @Override
    @Transactional(readOnly = true)
    public List<OwnerDto> findAll() {
        log.info("find all owner");
        return ownerRepository.findAll().stream()
                .map(OwnerServiceImpl::ownerToOwnerDto)
                .collect(Collectors.toList());

    }

    @Override
    @Transactional(readOnly = true)
    public OwnerDto findById(Long id) {
        isNullCheck(id, "ownerFind");
        log.info("find owner by id");
        Owner owner = ownerRepository.findById(id).orElse(null);
        return ownerToOwnerDto(owner);
    }

    @Override
    @Transactional
    public OwnerDto save(OwnerDto ownerDto) {
        isNullCheck(ownerDto, "ownerSave");
        log.info("save owner");
        Owner owner = ownerDtoToOwner(ownerDto);
        Owner updatedOwner = ownerRepository.save(owner);
        return ownerToOwnerDto(updatedOwner);
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        isNullCheck(id, "ownerDeleteById");
        log.info("delete owner by id");
        ownerRepository.deleteById(id);
    }

    @Synchronized
    public static Owner ownerDtoToOwner(OwnerDto ownerDto) {
        if (Objects.isNull(ownerDto)) {
            return null;
        }
        return Owner.builder()
                .id(ownerDto.getId())
                .firstName(ownerDto.getFirstName())
                .lastname(ownerDto.getLastname())
                .age(ownerDto.getAge())
                .phoneNumber(ownerDto.getPhoneNumber())
                .email(ownerDto.getEmail())
                .password(ownerDto.getPassword())
                .address(ownerDto.getAddress())
                .build();
    }

    @Synchronized
    public static OwnerDto ownerToOwnerDto(Owner owner) {
        if (Objects.isNull(owner)) {
            return null;
        }
        return OwnerDto.builder()
                .id(owner.getId())
                .firstName(owner.getFirstName())
                .lastname(owner.getLastname())
                .age(owner.getAge())
                .phoneNumber(owner.getPhoneNumber())
                .email(owner.getEmail())
                .password(owner.getPassword())
                .address(owner.getAddress())
                .build();
    }
}
