package com.exadel.insurance.service;

import com.exadel.insurance.dto.OwnerDto;

public interface OwnerService extends CrudService<OwnerDto, Long> {
}
