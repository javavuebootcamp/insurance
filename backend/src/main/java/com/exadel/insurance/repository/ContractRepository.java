package com.exadel.insurance.repository;

import com.exadel.insurance.entity.Contract;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ContractRepository extends CrudRepository<Contract, Long> {
    List<Contract> findAll();
}
