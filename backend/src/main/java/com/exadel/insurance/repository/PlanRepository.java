package com.exadel.insurance.repository;

import com.exadel.insurance.entity.Plan;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PlanRepository extends CrudRepository<Plan, Long> {
    List<Plan> findAll();
}
